import { Controller, Get, Query, Post, Body, Put, Param, Delete, UseFilters, UseGuards, UseInterceptors, BadRequestException } from '@nestjs/common';
import { CreateCatDto, UpdateCatDto, ListAllEntities } from './dtos/cat.dto';
import { CatsService } from './services/cats.service';
import { ICat } from './interfaces/cat.interface';
import { HttpExceptionFilter } from '../common/exception/http-exception.filter';
import { AuthGuard } from '../common/guard/auth.guard';
import { TimeoutInterceptor, TransformInterceptor } from '../common/interceptor/timeout.interceptor';
import { CatValidationPipe } from './pipes/cats.pipe.validation';
import { Cat } from './decorator/cats.decorator';

@Controller('cats')
@UseFilters(new HttpExceptionFilter())
@UseGuards(AuthGuard)
//@UseInterceptors(new TimeoutInterceptor())
@UseInterceptors(new TransformInterceptor())
export class CatsController {
    constructor(private catsService: CatsService) {}

    @Post()
    create(@Body(new CatValidationPipe()) createCatDto: CreateCatDto) {
        /*
        {
            "age": 3,
            "breed": "VN",
            "name": "tom"
        }
        */
        const cat : ICat = {
            id: 0,
            age: createCatDto.age,
            breed: createCatDto.breed,
            name: createCatDto.name
        }
        return this.catsService.create(cat)
    }

    @Get()
    findAll(@Query() query: ListAllEntities) {
        return this.catsService.findAll(query.skip, query.limit);
    }

    @Get(':id')
    findOne(@Param('id') id: number) : ICat {
        return this.catsService.findById(id);
    }

    @Put(':id')
    update(@Param('id') id: number, @Body(new CatValidationPipe()) updateCatDto: UpdateCatDto) : ICat {
        return this.catsService.updateById(id, updateCatDto);
    }

    @Delete(':id')
    remove(@Param('id') id: number) : ICat {
        return this.catsService.deleteById(id);
    }
}
