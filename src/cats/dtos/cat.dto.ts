import { IsString, IsInt } from 'class-validator';

export class CreateCatDto {
  @IsString()
  name: string;
  @IsInt()
  age: number;
  @IsString()
  breed: string;
}

export class UpdateCatDto {
  id: number;
  @IsString()
  name: string;
  @IsInt()
  age: number;
  @IsString()
  breed: string;
}

export class ListAllEntities {
  @IsInt()
  limit: number;
  @IsInt()
  skip: number;
}