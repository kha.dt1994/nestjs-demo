import { Test, TestingModule } from '@nestjs/testing';
import { CatsController } from './cats.controller';
import { ListAllEntities } from './dtos/cat.dto';
import { ICat } from './interfaces/cat.interface';
import { CatsService } from './services/cats.service';

describe('CatsController', () => {
  let catsController: CatsController;
  let catsService: CatsService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [CatsController],
      providers: [CatsService],
    }).compile();

    catsService = moduleRef.get<CatsService>(CatsService);
    catsController = moduleRef.get<CatsController>(CatsController);
  });

  describe('findAll', () => {
    it('should return an array of cats', async () => {
      const queryInput: ListAllEntities = {
        limit: 10,
        skip: 0
      }

      const result: ICat[] = [
        {
          id: 1,
          name: "Mon",
          breed: "VN",
          age: 3
        }];

      jest.spyOn(catsService, 'findAll').mockImplementation(() => result);
      var aaa = await catsController.findAll(queryInput);
      expect(aaa).toBe(result);
    });
  });

  describe('create', () => {
    it('create cat', async () => {
      const input = {
        name: "Mon",
        breed: "VN",
        age: 3
      }

      const result: ICat = {
        id: 1,
        name: "Mon",
        breed: "VN",
        age: 3
      };

      jest.spyOn(catsService, 'create').mockImplementation(() => result);

      expect(await catsController.create(input)).toBe(result);
    });
  });
});