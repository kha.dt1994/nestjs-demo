import { BadRequestException, Injectable } from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import { ICat } from './../interfaces/cat.interface';

@Injectable()
export class CatsService {
  private readonly cats: ICat[] = [];

  constructor(private moduleRef: ModuleRef) {
  }

  create(cat: ICat): ICat {
    const maxId = this.getMaxId();
    cat.id = (maxId || 0) + 1;
    this.cats.push(cat);
    return cat;
  }

  findAll(skip: number, take: number): ICat[] {
    return this.cats.slice(skip, take);
  }

  findById(id: number): ICat {
    const cat = this.cats.find(x => x.id == id);
    if (cat == null) throw new BadRequestException("Id not found"); 
    return cat;
  }

  updateById(id: number, cat: ICat): ICat {
    const index = this.cats.findIndex(x => x.id == id);
    if (index > 0) {
      this.cats[index].name = cat.name;
      this.cats[index].age = cat.age;
      this.cats[index].breed = cat.breed;
      return this.cats[index];
    }
    throw new BadRequestException("Id not found"); 
  }

  deleteById(id: number): ICat {
    var index = this.cats.findIndex(x => x.id == id);
    if (index > 0) {
      var cat = this.cats.find(x => x.id == id);
      this.cats.splice(index, 1);
      return cat;
    }
    throw new BadRequestException("Id not found");
  }

  private getMaxId() {
    if (this.cats.length == 0) {
      return 0;
    }
    const maxId = this.cats.reduce(
      (max, character) => (character.id > max ? character.id : max),
      this.cats[0].id
    );
    return maxId;
  }
}