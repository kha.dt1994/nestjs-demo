import { Request, Response, NextFunction } from 'express';

export function logger(req: Request, res: Response, next: NextFunction) {
  console.log(`Middleware Request...`);
  next();
  console.log(`Middleware Response...`);
};

export function logger1(req: Request, res: Response, next: NextFunction) {
  console.log(`Middleware1 Request...`);
  next();
  console.log(`Middleware1 Response...`);
};